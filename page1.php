<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tjmonsi
 * Date: 10/7/13
 * Time: 1:02 PM
 * To change this template use File | Settings | File Templates.
 */

// Process your information here

if (!empty($_POST)) {
    $user = $_POST['user'];
    //echo "hello";
} else {
    header("Location: index.php");
    /* Make sure that code below does not get executed when we redirect. */
    exit;
}

// save $user as cookie as a carry on data for the next pages
setcookie("user", $user, time()+(3600*3)); // time of expiration is 3 hours

?>
<html>
<head>
    <title>AutoComPaste Experiment</title>
</head>
<body>
<div>
    <h1>
       Experiment Pre-Questionnaire
    </h1>
	<p>The information captured individually as a result of your participation in this experiment will be kept 
	confidential and that there will be no possibility that you can be identified from or associated with any results
	or summaries of results, as presented or published subsequent to this experiment. </p>
</div>
<div>
    <h3>Participant No: <?php echo $user; ?> </h3>
    <form action="page2.php" method="post">
		<p>
			<span>Year of Birth (e.g. 1989)</span><input type="text" name="var1" /><br/>
		</p>
		<p>
			<span>Gender</span><br/>
			<input type="radio" name="var2" value="female">Female<br/>
			<input type="radio" name="var2" value="male">Male<br/>
		 </p>
		 <p><span>Semester of Study</span><br/>
			<select name="var3">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
			</select><br/>
		</p>
		<p><span>Describe your IT work environment setup. (E.g. Desktop with 2 screens, 2 keyboards, etc.)</span>
			<br/><textarea name="var4" rows="10" cols="30"></textarea><br/>
		</p>
		<p><span>What are some of the document and text editors you are familiar with?</span>
			<br/><textarea name="var5" rows="10" cols="30"></textarea><br/>
		</p>
		<p><span>How many windows do you open when you are working on a text document?</span><br/>
			<input type="radio" name="var6" value="2to3">2-3  at the same time<br/>
			<input type="radio" name="var6" value="5to10">5-10 at the same time<br/>
			<input type="radio" name="var6" value="10orMore">more than 10 at the same time<br/>
		</p>
		<p><span>How proficient are you with Keyboard Shortcuts?</span><br/>
			<input type="radio" name="var7" value="Average">Average<br/>
			<input type="radio" name="var7" value="5to10">Used it all the time<br/>
			<input type="radio" name="var7" value="10orMore">Rarely<br/>
		</p>
		<p><span>List some of the Keyboard Shortcuts you know for text editing.</span>
			<br/><textarea name="var8" rows="10" cols="30"></textarea><br/>
			<input id="submit" type="submit" value="submit">
		</p>
	</form>
</div>

</body>
</html>