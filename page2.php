<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tjmonsi
 * Date: 10/7/13
 * Time: 3:45 PM
 * To change this template use File | Settings | File Templates.
 */

// Process your information here

if (!empty($_POST)) {
    $demodata = $_POST;
    foreach ($_POST as $key => $value) {
        setcookie("demodata_".$key, $value, time()+(3600*3));
    }

    //echo "hello";
} else {
    header("Location: index.php");
    /* Make sure that code below does not get executed when we redirect. */
    exit;
}

if (!isset($_COOKIE["user"])){
    $message = "Please use a username";
    header("Location: index.php?message=".$message);
    exit;
}

$user = $_COOKIE["user"];
?>

<html>
<head>
    <title>AutoComPaste Experiment</title>
</head>
<body>
<div>
    <p>Please click the Next button.</p>
<form action="page3.php" method="post">

    <span>Interface</span><br/>
    <input type="radio" name="interface" value="acp" <?php if(intval($user) < 5) echo 'checked'; else echo 'disabled';?>>ACP<br/>
    <input type="radio" name="interface" value="xwindow" <?php if(intval($user) >= 5) echo 'checked'; else echo 'disabled';?>>XWindow<br/>
    <input id="submit" type="submit" value="Next">
</form>
</div>

</body>
</html>