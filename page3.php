<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tjmonsi
 * Date: 10/7/13
 * Time: 3:59 PM
 * To change this template use File | Settings | File Templates.
 */

if (!isset ($_COOKIE["interface"]) && !isset ($_COOKIE["block"])) {
    if (!empty($_POST)) {
        $interface = $_POST["interface"];
		$block = 1;
        setcookie("interface", $interface, time()+(3600*3));
		setcookie("block", $block, time()+(3600*3));
        //echo "hello";
    } else {
        header("Location: index.php");
        /* Make sure that code below does not get executed when we redirect. */
        exit;
    }
} else {
    $interface = $_COOKIE["interface"];
	$block = $_COOKIE["block"];
    if ((strcmp($interface, "acp")==0 && $block == 1)) {
        $interface = "xwindow";
		$block = 1;
    }else if((strcmp($interface, "xwindow")==0 && $block == 1)){
		$interface = "acp";
		$block = 2;
	}else if ((strcmp($interface, "xwindow")==0 && $block == 2)) {
        $interface = "acp";
		$block = 1;
    }else if ((strcmp($interface, "acp")==0 && $block == 2)) {
        $interface = "xwindow";
		$block = 2;
    } else{
	}
	setcookie("interface", $interface, time()+(3600*3));
	setcookie("block", $block, time()+(3600*3));
}

if (!isset($_COOKIE["user"])){
    $message = "Please use a username";
    header("Location: index.php?message=".$message);
    exit;
}

$user = $_COOKIE["user"];

if (strcmp($interface, "acp")==0) {
    $msg = "<h2>Please use only the <u>AutoComPaste</u> text editing technique.</h2> Please ensure you have read and understand the instructions before continuing. Otherwise, you may seek assistance.";
    $acpflag = "true";
} else {
    $msg = "<h2>Please use only the <u>xWindows</u> text editing technique.</h2> Please ensure you have read and understand the instructions before continuing. Otherwise, you may seek assistance.";
    $acpflag = "false";
}

require_once("external_files.php");

?>
<html>
<head>
    <title>AutoComPaste Experiment</title>
</head>
<body>
<div>
<h2>General Instructions</h2>
    <p>
You are required to read and follow the instructions correctly at all times when performing all tasks. 
Please ensure that you have fill up the consent form, pre-questionnaire and gone through sufficient practise on both texts editing technique and the experiment interface before continuing.
    </p>
    <p>
       <?php echo $msg; ?>
    </p>
    <form action="interface1.php?user=<?php echo $user; ?>&acp=<?php echo $acpflag; ?>&data=<?php echo $data; ?>&jslist=<?php echo $jslist; ?>&tasklist=<?php echo $tasklist; ?>&block=<?php echo $block; ?>" method="post">
        <input id="submit" type="submit" value="start">
    </form>
</div>


</body>
</html>
