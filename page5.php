<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tjmonsi
 * Date: 10/7/13
 * Time: 8:20 PM
 * To change this template use File | Settings | File Templates.
 */

if (!isset($_COOKIE["user"])){
    $message = "Please use a username";
    header("Location: index.php?message=".$message);
    exit;
}

$prelimdata = "";
foreach ($_COOKIE as $key=>$value) {
	if( !(strcmp("interface", $key)==0) && !(strcmp("xwindow", $key)==0) && !(strcmp("acp", $key)==0))
    $prelimdata = $prelimdata.trim($value).",";
}
?>

<html>
<head>
    <title>Copy-paste Experiment</title>
</head>
<body>
<div>
<script>
function saveTextAsFile()
{	
	var userData  = "<?php echo $prelimdata; ?>";
	var textToWrite = "USER_PROFILE\n"+userData+"\n\nUSER_DATA\n"+localStorage.getItem("I=acp;B=1")+localStorage.getItem("I=acp;B=2")+localStorage.getItem("I=xwindow;B=1")+localStorage.getItem("I=xwindow;B=2");
	var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
	var fileNameToSaveAs = "result"

	var downloadLink = document.createElement("a");
	downloadLink.download = fileNameToSaveAs;
	downloadLink.innerHTML = "Download File";
	if (window.webkitURL != null)
	{
		downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
	}
	else
	{
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		downloadLink.onclick = destroyClickedElement;
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
	}
	downloadLink.click();
	localStorage.clear();
}

</script>
	<button onclick="saveTextAsFile()">Save File</button>
	<h2><a href="reset.php">Reset</a></h2>
</div>
</body>
</html>
