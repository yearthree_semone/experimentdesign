<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tjmonsi
 * Date: 10/7/13
 * Time: 7:51 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * CHANGE YOUR LOADING OF EXTERNAL FILES HERE
 */
$jslist = "extrajs"; //  extrajs/extrajs.txt
$data = "data"; //  data/data.txt
$tasklist = "tasks";//   task/tasks.json

if($user == 1 || $user == 5){
	if($block == 1){
		$tasklist="p1b1";
	}else{
		$tasklist="p1b2";
	}
}else if($user == 2 || $user == 6){
	if($block == 1){
		$tasklist="p2b1";
	}else{
		$tasklist="p2b2";
	}
}else if($user == 3 || $user == 7){
	if($block == 1){
		$tasklist="p3b1";
	}else{
		$tasklist="p3b2";
	}
}else if($user == 4 || $user == 8){
	if($block == 1){
		$tasklist="p4b1";
	}else{
		$tasklist="p4b2";
	}
}else{
	$tasklist = "tasks";
}