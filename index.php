<?php
/**
 * Created by JetBrains PhpStorm.
 * User: tjmonsi
 * Date: 10/7/13
 * Time: 11:32 AM
 * To change this template use File | Settings | File Templates.
 */

?>
<html>
<head>
    <title>Copy-paste Experiment</title>
</head>
<body>
    <div>
		<h1>
            Good Day!
        </h1>
		<p>
		Thank you for the time to participate in this experiment. Please ensure that you have signed the
		<em>Experiment Participation Consent Form</em> before you begin the experiment.
		</p>
        <p>
            Please write your participant number ID given by your experimenter:
        </p>
    </div>
    <div>
        <form action="page1.php" method="post">
            <span>Participant ID:</span><input type="text" name="user" />
            <input id="submit" type="submit" value="start">
        </form>
    </div>

</body>
</html>